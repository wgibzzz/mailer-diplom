class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /contacts
  # GET /contacts.json
  def index
    logger.debug params
    if params[:group_by].nil?
      @contacts = Contact.all
    elsif params[:group_by].present?
      if params[:group_by] == 'group'
        @contacts = {
            :contacts => Relation.joins(:contact, :group).select('"relations"."contact_id", "relations"."group_id", "contacts"."name", "contacts"."email"'),
            :groups => Group.all
        }
      elsif params[:group_by] == 'organisation'
        @contacts = {
            :contacts => Contact.select(:id, :organisation_id, :name).all,
            :organisations => Organisation.select(:id, :name).all
        }
      end
    elsif params[:organisation_id].present?
      @organisations = Organisation.find(params[:organisation_id].split(','))
      @contacts = []
      @organisations.each do |org|
        @contacts.concat org.contacts
      end
    end
    respond_to do |format|
      format.html { render :index }
      format.json { render :json => @contacts }
    end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to root_path, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { redirect_to root_path, notice: @contact.errors }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to root_path, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { redirect_to root_path, notice: @contact.errors }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:name, :email, :organisation_id, :group_ids => [])
  end
end
