class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :authenticate_user!
  protect_from_forgery with: :exception

  # noinspection RailsChecklist01
  def index
    @organisation = Organisation.new
    @group = Group.new
    @contact = Contact.new
    @organisations = Organisation.order(:name => :desc).all
    @groups = Group.all
    @contacts = Contact.order(:organisation_id => :desc).all
    render :index
  end

  def mailer
    render :mailer
  end

  def sendmail
    @emails = Contact.find(params[:user_ids]).map(&:email)
    @emails.each do |email|
      ContactsMailer.dispatch(email, params[:subject], params[:body]).deliver_later
    end
    redirect_to root_path, :flash => {:info => 'Сообщения начали рассылаться'}
  end
end
