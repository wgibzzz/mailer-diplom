class OrganisationRelation < ActiveRecord::Base
  belongs_to :organisation
  belongs_to :group
end
