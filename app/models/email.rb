class Email < ActiveRecord::Base
  belongs_to :contact

  validates_presence_of :email
end
