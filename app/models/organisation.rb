class Organisation < ActiveRecord::Base
  has_many :contacts
  has_many :organisation_relations
  has_many :groups, :through => :organisation_relations

  validates_presence_of :name
end
