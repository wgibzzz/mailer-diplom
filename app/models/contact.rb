class Contact < ActiveRecord::Base
  belongs_to :organisation
  has_many :emails
  has_many :relations
  has_many :groups, :through => :relations

  validates_presence_of :name, :email
  validates :email, :email => true
end
