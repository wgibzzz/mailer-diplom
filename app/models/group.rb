class Group < ActiveRecord::Base
  has_many :relations
  has_many :contacts, :through => :relations
  has_many :organisation_relations
  has_many :organisations, :through => :organisation_relations

  validates_presence_of :name
end
