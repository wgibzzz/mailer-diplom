class ContactsMailer < ApplicationMailer
  def dispatch(contact, subject, body)
    @maildata = {
        :contact => contact,
        :subject => subject,
        :body => body
    }
    mail(to: contact, subject: subject)
  end
end
