xhr = false
select_organisations = false
$select_organisations = false
select_contacts = false
$select_contacts = false

$select_organisations = $('select#group_organisation_ids').selectize({
  plugins: ['remove_button'],
  onChange: (value) ->
    if !value.length then return;
    select_contacts.disable();
    select_contacts.clearOptions();
    select_contacts.load((callback) ->
      xhr && xhr.abort();
      xhr = $.ajax({
        url: '/organisations/' + value + '/contacts.json',
        success: (results) ->
          select_contacts.enable();
          callback(results);
          return;
        error: () ->
          callback();
          return;

      })
    )
})

$select_contacts = $('select#group_contact_ids').selectize({
  plugins: ['remove_button'],
  valueField: 'id',
  labelField: 'name'
})

select_contacts = $select_contacts[0].selectize if $select_contacts.length > 0
select_organisations = $select_organisations[0].selectize if $select_organisations.length > 0

select_contacts.disable() if $select_contacts.length > 0