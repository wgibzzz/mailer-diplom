# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#contact_organisation_id
xhr = false
select_organisation = false
$select_organisation = false
select_group = false
$select_group = false
select_contact = false
$select_contact = false
$selected_data = []
############# Begin index inputs
$select_organisation = $('select#contact_organisation_id').selectize({
  onChange: (value) ->
    if !value.length then return;
    select_group.disable();
    select_group.clearOptions();
    select_group.load((callback) ->
      xhr && xhr.abort();
      xhr = $.ajax({
        url: '/organisations/' + value + '/groups.json',
        success: (results) ->
          select_group.enable();
          callback(results);
          return;
        error: () ->
          callback();
          return;

      })
    )
})

$select_group = $('select#contact_group_ids').selectize({})

select_group = $select_group[0].selectize if $select_group.length > 0
select_organisation = $select_organisation[0].selectize if $select_organisation.length > 0

select_group.disable() if $select_group.length > 0

############# End of index inputs
############# Begin mailer inputs
$select_uids = $('select#user_ids').selectize({})
select_uids = $select_uids[0].selectize if $select_uids.length > 0

$selectizeValAdd = (e) ->
  select_uids.addItems e
  return;

$selectizeValRemove = (val, $item) ->
  if val not in select_contact_id.items and val not in select_g_contact_id.items and val not in select_o_contact_id.items
    select_uids.removeItem val
  return;


$select_contact_id = $('select#contact_id').selectize({})
select_contact_id = $select_contact_id[0].selectize if $select_contact_id.length > 0

$select_g_contact_id = $('select#contact_by_group').selectize({})
select_g_contact_id = $select_g_contact_id[0].selectize if $select_g_contact_id.length > 0

$select_o_contact_id = $('select#contact_by_org').selectize({})
select_o_contact_id = $select_o_contact_id[0].selectize if $select_o_contact_id.length > 0
select_contact_id.on 'item_add', $selectizeValAdd if $select_contact_id.length > 0
select_g_contact_id.on 'item_add', $selectizeValAdd if $select_g_contact_id.length > 0
select_o_contact_id.on 'item_add', $selectizeValAdd if $select_o_contact_id.length > 0
select_contact_id.on 'item_remove', $selectizeValRemove if $select_contact_id.length > 0
select_g_contact_id.on 'item_remove', $selectizeValRemove if $select_g_contact_id.length > 0
select_o_contact_id.on 'item_remove', $selectizeValRemove if $select_o_contact_id.length > 0
$('#dispatch_form').on 'submit', (e) ->
  select_uids.enable()