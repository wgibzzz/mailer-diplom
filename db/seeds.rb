# Its a MAGIC!11 YAY
Faker::Config.locale = 'ru'

Organisation.create(
    [
        {name: 'Администрация'},
        {name: 'Дом культуры'},
        {name: 'ГОВД'},
        {name: 'ЛТВ'}
    ]
)

groups = Group.create(
    [
        {name: 'Жил. отдел'},
        {name: 'По связям с общественностью'},
        {name: 'Юридический'},
        {name: 'Экономический'},
        {name: 'IT-отдел'},
        {name: 'Экономический'},
        {name: 'Отдел по работе со СМИ'},
        {name: 'IT-отдел'},
        {name: 'Отдел по обращениям граждан'},
        {name: 'Отдел по работе с молодежью'},
        {name: 'Отдел СМИ'},
        {name: 'Отдел по общественным связям'}
    ]
)
groups[0..4].each do |group|
  group.organisations << Organisation.find_by_name('Администрация')
  group.save
end
groups[5..8].each do |group|
  group.organisations << Organisation.find_by_name('Дом культуры')
  group.save
end
groups[9..10].each do |group|
  group.organisations << Organisation.find_by_name('ГОВД')
  group.save
end
groups[11..12].each do |group|
  group.organisations << Organisation.find_by_name('ЛТВ')
  group.save
end

Group.all.each do |group|
  contact = Contact.new(name: 'Соломатин А. C.', email: Faker::Internet.safe_email, organisation: group.organisations.first)
  group.contacts << contact
  contact.save
  rand(6).times do
    contact = Contact.create(name: Faker::Name.name, email: Faker::Internet.safe_email, organisation: group.organisations.first)
    group.contacts << contact
    contact.save
  end
end
User.create(email: 'admin@example.com', password: '12345678', password_confirmation: '12345678')