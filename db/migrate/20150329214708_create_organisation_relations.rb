class CreateOrganisationRelations < ActiveRecord::Migration
  def change
    create_table :organisation_relations do |t|
      t.references :organisation, index: true, foreign_key: true
      t.references :group, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
