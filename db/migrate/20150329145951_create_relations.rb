class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.references :group, index: true, foreign_key: true
      t.references :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
