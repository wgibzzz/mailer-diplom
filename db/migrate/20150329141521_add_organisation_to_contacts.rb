class AddOrganisationToContacts < ActiveRecord::Migration
  def change
    add_reference :contacts, :organisation, index: true, foreign_key: true
  end
end
